#parse JSON Data.

import json
import urllib
import urllib2

f = open('json/phase3.json').read();
json_phase4_data = json.loads(f)
exploits = json_phase4_data["exploits"]

count = 0
valid_exploits = []

def record_valid_expoit(exploit) :
	global valid_exploits
	valid_exploits.insert(0, exploit)

def construct_valid_exploit_json(valid_exploits) :
	jsonObj = {}
	if len(valid_exploits) > 0 :
		jsonObj["exploits"] = valid_exploits
	else :
		jsonObj["exploits"] = []
	f = open('json/testJson.json', 'w')
	jsonString = json.dumps(jsonObj)
	f.write(jsonString)

for exp in exploits:
	count = count + 1
	#print exp
	print ">>>>>>>>>>>>>>>>>>>>>>>>>> Script---------------------------------------------------------------------" + str(count)

	if(exp["method"] == 'GET') :
		script = exp["url"] + '?'
		params = exp["params"]
		paramCount = 0
		for param in params:
			paramCount = paramCount + 1
			script = script + param["key"] + '=' + param["value"]
			if(paramCount < len(params)):
				script = script + "&"

		# >>>>>>>>>>>>>>>>>>>>> Writing into a file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		#this_page = urllib2.urlopen(script).read()
		#print this_page
		#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TODO write the script in a file and execute the script
		record_valid_expoit(exp)

	if(str(exp["method"]) == 'POST') :
		data = {}
		paramCount = 0
		for param in params:
			paramCount = paramCount + 1

			if(type(param["key"]) is unicode) :
				key = '\'' + str(param["key"]) + '\''
			if(type(param["key"]) is int) :
				key = int(param["key"])
			
			if(type(param["value"]) is unicode) :
				value = '\'' + str(param["value"]) + '\''
			if(type(param["value"]) is int) :
				value = int(param["value"])
			
			data.update({key:value})
			
		#req = urllib2.Request(url='\"' + str(exp["url"]) + '\"', data = urllib.urlencode(data))
		#this_page = urllib2.urlopen(req).read()
		#print this_page
		record_valid_expoit(exp)

	#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TODO write the script in a file and execute the script


if len(valid_exploits) > 0 :
	print valid_exploits
else :
	print 'No Valid Exploits Found!'

construct_valid_exploit_json(valid_exploits)