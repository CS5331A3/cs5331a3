# Imports
import urllib, urllib2
from BeautifulSoup import BeautifulSoup,SoupStrainer

def dict_add(d1,d2):
	"""
		Flatten 2 dictionaries
	"""
	d={}
	if len(d1):
		for s in d1.keys():
			d[s] = d1[s]
	if len(d2):
		for s in d2.keys():
			d[s] = d2[s]
	return d

def split_query(query_string):
	"""
		Split the param1=value1&param2=value2 into a dictionary
	"""
	try:
		d = dict([x.split('=') for x in query_string.split('&') ])
	except ValueError:
		d = {}
	return d

def get_html_content(url, value):
	"""
		Fetch HTML content
			url - Clean URL without any parameters
			value - parameters
	"""
	data = urllib.urlencode(value)

	# Submit the form
	req = urllib2.Request(url, data)
	rsp = urllib2.urlopen(req)
	
	return rsp.read()

def parse_HTML(url, htmlContent):
	"""
		Parse the HTML/XHTML code to get POST/GET requests parameters
	"""
	urlGlobalList = list()
	forms = SoupStrainer('form')
	input = SoupStrainer('input')

	listForm = [tag for tag in BeautifulSoup(htmlContent, parseOnlyThese=forms)]

	for f in listForm:
		methodVal = 'GET'
		if (f.has_key('method') or f.has_key('method')):
			methodVal = f['method'].upper()

		if (methodVal == 'POST'):
			listInput = [tag for tag in BeautifulSoup(str(f), parseOnlyThese=input)]

			tmpUrlDict = dict()	
			tmpDict = dict()		
			for i in listInput:
				try:
					value = i['value']
				except KeyError:
					value = ''
				try:
					name = i['name']
				except KeyError:
					name = '-1'
					value= ''
					continue

				name = str(name)
				value = str(value)

				if (name != '-1'):
					tmpUrlDict['url'] = url
					tmpUrlDict['method'] = methodVal
					tmpDict = dict_add(tmpDict, {name : value})
					tmpUrlDict['params'] = tmpDict

			if (bool(tmpUrlDict)):
				urlGlobalList.append(tmpUrlDict)

	return urlGlobalList