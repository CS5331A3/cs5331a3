import json

def stage2_exploit_generation(stage1_injection_file, stage2_in_exploitsFile, stage2_Out_JsonFile):

	injection_points = []
	f1 = open(stage1_injection_file, 'r').read()
	injection_points = json.loads(f1)

	exploit_payloads = []
	f2 = open(stage2_in_exploitsFile, 'r').read()
	exploit_payloads = json.loads(f2)

	pot_exploits = []

	for pot_pay in exploit_payloads:

		for inject_point in injection_points :

			url = inject_point["url"]
			method = inject_point["method"]
			params = inject_point["params"]

			for param_key, param_value in params.iteritems():
				exploit = {}
				exploit.update({"url":url})
				exploit.update({"method":method})
				paramsData = []
				for p_k, p_v in params.iteritems():
					data = {}
					data.update({"key":p_k})
					if p_k==param_key:
						data.update({"value":pot_pay})
					else:
						data.update({"value":p_v})
					paramsData.insert(0, data)
				exploit.update({"params":paramsData})
				pot_exploits.insert(0, exploit)

	jsonObj = {}
	jsonObj["exploits"] = exploits
	f = open(stage2JsonFile, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	f.write(jsonString)
	f.close()

stage2_exploit_generation()