from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from urlparse import urlparse
# from scrapy.contrib.spiders import CrawlSpider, Rule
# from scrapy.spider import Spider
from scrapy.spider import BaseSpider
from crawler.items import LinkItem
from scrapy.http import Request
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import scrapy
from scrapy.http import FormRequest
from scrapy import log
from loginform import fill_login_form
import json
#import injection
import urllib, urllib2
from BeautifulSoup import BeautifulSoup,SoupStrainer

class NewSpider(BaseSpider):
    name =  'newattacks'
    allowed_domains=[]#["app5.com"]
    start_urls = []#['https://app5.com/www/index.php']
    item_urls = []
    all_urls = []
    login_user = 'admin'
    login_pass = 'admin'
    json_objects = []
    patterns_in_exploits = []
    attacksurls=[]
    validExpUrls = []
    handle_httpstatus_list = [500, 404,301]
    
    #def parse(self, response):
    #    args, url, method = fill_login_form(response.url, response.body, self.login_user, self.login_pass)
     #   return FormRequest(url, method=method, formdata=args, dont_filter=True,callback=self.after_login)
      #  return FormRequest.from_response(
       #      response,            
        #     formdata={'user': 'admin', 'password': 'admin'},
              # method="POST",
         #      dont_filter=True,
          #   callback=self.after_login
         #)
   #      return Request(url=self.start_urls[0],
    #                  callback=self.parse_page)
        #return [scrapy.FormRequest(self.start_urls[0], 
         #   formdata={'Username': 'admin', 'Password': 'admin'},
          #   callback=self.after_login)]
    
    def parse(self, response):
      # args, url, method = fill_login_form(response.url, response.body, self.login_user, self.login_pass)
       #return FormRequest(url, method=method, formdata=args, dont_filter=True,callback=self.after_login)
      args, url, method, name , number = fill_login_form(response.url, response.body, self.login_user, self.login_pass)
      if name:
        yield FormRequest.from_response(response, method=method, formdata=args, formname=name,  dont_filter=True,callback=self.after_login)
      else:
        yield FormRequest.from_response(response, method=method, formdata=args, formnumber=number,  dont_filter=True,callback=self.after_login)

    def parse1(self, response):
      return FormRequest.from_response(response,  formdata={'user': 'admin', 'password': 'admin'},  dont_filter=True, callback=self.payload_generation("Stage2.json"))

    def __init__(self,filename = None):
        temp = []
        if filename:
            with open(filename, 'r') as f:
                temp = f.readlines()

        self.start_urls.append(temp[0].strip())
        self.allowed_domains.append(temp[1].strip())
        f = open("pattern.json").read();
        patterns_in_exploits_data = json.loads(f)
        self.patterns_in_exploits = patterns_in_exploits_data["pattern"]
        temp =[]
        with open("/home/user/Desktop/CSA3/cs5331a3/Stage3_Out/b4_valid_urls/app7_expUrlFile.txt", 'r') as f1:
                temp = f1.readlines() 
        self.attacksurls = temp
        dispatcher.connect(self.spider_closed, signals.spider_closed)               

#        dispatcher.connect(self.spider_closed, signals.spider_closed)
    def isURLAttacked(self, url):
        for t  in self.item_urls:    
          if t in url:
            return True
        return False

    def load_pot_exploits(self, pot_exploits_file):
      f = open(pot_exploits_file, 'r').read();
      pot_exploits_data = json.loads(f)
      pot_exploits = pot_exploits_data["exploits"]
      return pot_exploits

    def load_pattens_in_exploits(self, patterns_in_exploits_file):
      f = open(patterns_in_exploits_file, 'r').read();
      patterns_in_exploits_data = json.loads(f)
      patterns_in_exploits = patterns_in_exploits_data["pattern"]
      return patterns_in_exploits

    def after_login(self, response):

        self.pot_exploits = self.load_pot_exploits('Stage2.json')
        self.patterns_in_exploits = self.load_pattens_in_exploits('pattern.json')

        if "The username/password combination you have entered is invalid" in response.body:
            self.log("Login failed", level=log.ERROR)
            return        
        # continue scraping with authenticated session...
        elif "logout" in response.body:
            print "This is response url " +  response.url
            self.log("Login succeed!", level=log.DEBUG)
            return Request(url=response.url,
                           callback=self.parse_page)
        else:
            print "Error1111"
            print self.attacksurls
            return Request(url=response.url,
                           callback=self.parse_page)

    def parse_page(self, response):
        print ">>> Response parse page res-call>>> " + response.url 

        if (response.status == 500) or (response.status == 404):
          print ">>> Response parse page res-call-body >>> " + response.body

        #print ">>> Response Body: " + response.body
        print "-------------------------------------------------------"
        for pattern in self.patterns_in_exploits:
          if str(pattern) in response.body:
            print str(pattern)
            self.validExpUrls.append(response.url)
            f = open('Stage3Out1.txt', 'a')
            f.write(response.url + '\n')
            f.close()
            break
        notAttacked = False
        notAttackedUrl = ""
        for link in self.attacksurls:
          link = link.strip()
          if(not self.isURLAttacked(link)):
            self.item_urls.insert(0, link)
            notAttackedUrl = link
            notAttacked = True
            break
        if (notAttacked):
          print ">>> Attack >>> " + notAttackedUrl
          notAttackedUrl = notAttackedUrl.strip()
          yield Request(url=notAttackedUrl, callback=self.parse_page)
            #break
        
    def spider_closed(self, spider):

      f = open('Stage3Out.txt', 'a')
      for item in self.validExpUrls :
        f.write(item + '\n')
      f.close()
      
    def collect_item(self, item):
        return item
        