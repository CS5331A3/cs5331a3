import json

def load_potential_payloads():
	potential_payloads = []
	f = open("json/exploits.json").read()
	potential_payloads = json.loads(f)
	return potential_payloads

def load_injection_points():
	injection_points = []
	f = open("json/phase1.json").read()
	injection_points = json.loads(f)
	return injection_points

def payload_generation(file):
	potential_payloads = load_potential_payloads()
	injection_points = load_injection_points()

	pot_exploits = []

	for pot_pay in potential_payloads:

		for inject_point in injection_points :

			url = inject_point["url"]
			method = inject_point["method"]
			params = inject_point["params"]

			for param_key, param_value in params.iteritems():
				exploit = {}
				exploit.update({"url":url})
				exploit.update({"method":method})
				paramsData = []
				for p_k, p_v in params.iteritems():
					data = {}
					data.update({"key":p_k})
					if p_k==param_key:
						data.update({"value":pot_pay})
					else:
						data.update({"value":p_v})
					paramsData.insert(0, data)
				exploit.update({"params":paramsData})
				pot_exploits.insert(0, exploit)

	generate_payload_json(file, pot_exploits)

def generate_payload_json(file, exploits):
	jsonObj = {}
	jsonObj["exploits"] = exploits
	f = open(file, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	f.write(jsonString)
	f.close()

payload_generation('json/testJson.json')