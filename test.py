import json

def stage3_comp_exploit_json (exploit1, exploit2):

	if (str(exploit1["url"]) not in str(exploit2["url"])):
		return False
	
	if(exploit1["method"] not in exploit2["method"]):
		return False
	
	params1 = exploit1["params"]
	params2 = exploit2["params"]

	if len(params1) != len(params2):
		return False
	
	paramsMatch = True
	for p1 in params1:
		match = False
		for p2 in params2:
			ctr1 = str(p1["key"]) + str(p1["value"])
			ctr2 = str(p1["key"]) + str(p2["value"])
			if ctr1 in ctr2:
				match = True
				break
		paramsMatch = paramsMatch and match
		if (not paramsMatch):
			break
	return paramsMatch

def stage3_remove_duplicates_in_json(jsonFile):

	fobj = open(jsonFile, 'r')
	json_data = json.loads(fobj.read())

	new_list=[]
	for item in json_data["exploits"]:

		if len(new_list)==0:
			new_list.append(item)
		else:
			match = False
			for new_item in new_list:
				match = stage3_comp_exploit_json(item, new_item)
				if (match):
					break
			if (not match):
				new_list.append(item)

	jsonObj = {}
	jsonObj["exploits"] = new_list
	fobj = open(jsonFile, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	fobj.write(jsonString)
	fobj.close()

stage3_remove_duplicates_in_json('noDupJson.json')