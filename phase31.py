
import json
import urllib2

def stage3_comp_exploit_json (exploit1, exploit2):

	if (str(exploit1["url"]) not in str(exploit2["url"])):
		return False
	
	if(exploit1["method"] not in exploit2["method"]):
		return False
	
	params1 = exploit1["params"]
	params2 = exploit2["params"]

	if len(params1) != len(params2):
		return False
	
	paramsMatch = True
	for p1 in params1:
		match = False
		for p2 in params2:
			ctr1 = str(p1["key"]) + str(p1["value"])
			ctr2 = str(p1["key"]) + str(p2["value"])
			if ctr1 in ctr2:
				match = True
				break
		paramsMatch = paramsMatch and match
		if (not paramsMatch):
			break
	return paramsMatch

def stage3_remove_duplicates_in_json(jsonFile):

	fobj = open(jsonFile, 'r')
	json_data = json.loads(fobj.read())

	new_list=[]
	for item in json_data["exploits"]:

		if len(new_list)==0:
			new_list.append(item)
		else:
			match = False
			for new_item in new_list:
				match = stage3_comp_exploit_json(item, new_item)
				if (match):
					break
			if (not match):
				new_list.append(item)

	jsonObj = {}
	jsonObj["exploits"] = new_list
	fobj = open(jsonFile, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	fobj.write(jsonString)
	fobj.close()

def load_pot_exploits(pot_exploits_file):
	f = open(pot_exploits_file, 'r').read();
	pot_exploits_data = json.loads(f)
	pot_exploits = pot_exploits_data["exploits"]
	return pot_exploits

def load_pattens_in_exploits(patterns_in_exploits_file):
	f = open(patterns_in_exploits_file, 'r').read();
	patterns_in_exploits_data = json.loads(f)
	patterns_in_exploits = patterns_in_exploits_data["pattern"]
	return patterns_in_exploits

def construct_exploit(pot_exploit, patterns_in_exploits):
	url = pot_exploit["url"]
	params = pot_exploit["params"]

	if(pot_exploit["method"] == "GET"):
		return construct_GET_exploit(url, params, patterns_in_exploits)
	else:
		return construct_POST_exploit()

def construct_valid_exploit_json(valid_exploits) :
	jsonObj = {}
	if len(valid_exploits) > 0 :
		jsonObj["exploits"] = valid_exploits
	else :
		jsonObj["exploits"] = []
	f = open('json/validExploitJson.json', 'w')
	jsonString = json.dumps(jsonObj)
	f.write(jsonString)
	f.close()

	stage3_remove_duplicates_in_json('json/validExploitJson.json')

def construct_GET_exploit(url, params, patterns_in_exploits, valid_exploits):
	valid = False
	payload = url;
	paramCount = len(params)
	if (paramCount>0):
		payload = payload + "?"
	for param in params:
		paramCount = paramCount - 1
		payload = payload + param["key"] + '=' + param["value"]
		if(paramCount > 0):
			payload = payload + "&"
	return validate_GET_payload(payload, patterns_in_exploits)

def validate_GET_payload(payload, patterns_in_exploits):
	content = urllib2.urlopen(payload)
	reply = content.read()

	if (LFI_Validation(reply, patterns_in_exploits) or RFI_Validation(reply, patterns_in_exploits)):
		return True
	else:
		return False

def LFI_Validation(body, patterns):
	for pattern in patterns:
		if pattern in body:
			return True
	return False

def RFI_Validation(body, patterns):
	return False

pot_exploits = load_pot_exploits('json/phase3.json')
print pot_exploits

patterns_in_exploits = load_pattens_in_exploits('json/pattern.json')
print patterns_in_exploits

print "--------->>>>>>>>>>>>>>>>>---------------<<<<<<<<<<<<<<<<<<----------------"

valid_exploits = []
for pot_exploit in pot_exploits:
	if (construct_exploit(pot_exploit, patterns_in_exploits)):
		valid_exploits.insert(0, pot_exploit)

print valid_exploits

construct_valid_exploit_json(valid_exploits)

