#parse JSON Data.

import json

f = open('json/phase4.json').read();
json_phase4_data = json.loads(f)
exploits = json_phase4_data["exploits"]

count = 0
for exp in exploits:
	count = count + 1
	#print exp
	print ">>>>>>>>>>>>>>>>>>>>>>>>>> Script---------------------------------------------------------------------" + str(count)

	if(exp["method"] == 'GET') :
		script = exp["url"] + '?'
		params = exp["params"]
		paramCount = 0
		for param in params:
			paramCount = paramCount + 1
			script = script + param["key"] + '=' + param["value"]
			if(paramCount < len(params)):
				script = script + "&"
	if(exp["method"] == 'POST') :
		script = "import urllib\n"
		script = script + "import urllib2\n"
		script = script + "data = { "
		paramCount = 0
		for param in params:
			paramCount = paramCount + 1

			if(type(param["key"]) is unicode) :
				script = script + '\'' + param["key"] + '\''
			if(type(param["key"]) is int) :
				script = script + param["key"]
			
			script = script + ':'
			
			if(type(param["value"]) is unicode) :
				script = script + '\'' + param["value"] + '\''
			if(type(param["value"]) is int) :
				script = script + param["value"]
			
			if(paramCount < len(params)):
				script = script + ','
			script = script + '}\n'
		script = script + 'req = urllib2.Request(url=\"' + exp["url"] + '\",\ndata=urllib.urlencode(data))\n'
		script = script + 'urllib2.urlopen(req)\nthis_page=response.read()\nprint this_page'
	print script