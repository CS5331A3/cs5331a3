import json

def load_potential_payloads():
	potential_payloads = []
	f = open('json/exploits.json').read();
	potential_payloads = json.loads(f)
	return potential_payloads

def load_injection_points():
	injection_points = []
	f = open('json/phase1.json').read();
	injection_points = json.loads(f)
	return injection_points

def payload_generation(file):
	potential_payloads = load_potential_payloads()
	injection_points = load_injection_points()

	pot_exploits = []

	for pot_pay in potential_payloads:

		for inject_point in injection_points :

			url = inject_point["url"]
			method = inject_point["method"]
			params = inject_point["params"]

			exploit = {}
			exploit.update({"url":url})
			exploit.update({"method":method})
		
			paramArray = []
			for param in params:
				data = {}
				data.update({"key":param})
				data.update({"value":pot_pay})
				paramArray.insert(0, data)
			exploit.update({"params":paramArray})
			pot_exploits.insert(0, exploit)

	generate_payload_json(file, pot_exploits)

def generate_payload_json(file, exploits):
	jsonObj = {}
	jsonObj["exploits"] = exploits
	f = open(file, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	f.write(jsonString)
	f.close()

"""
		payload = ""
		if (method == "GET") :
			get_payload_generation(url, params, potential_payloads)
		else :
			post_payload_generation()

def get_payload_generation(url, params, potential_payloads):
	payload = url
	if (len(potential_payloads) > 0):
		for pot_pay in potential_payloads :
			payload = url
			if (len(params) > 0):
				payload = payload + '?'
				numparams = len(params)
				for param in params:
					payload = payload + param + "=" + pot_pay
					if (numparams > 1):
						payload = payload + "&"
						numparams = numparams - 1
			print payload

def post_payload_generation(url, params):
	return payload

"""

payload_generation('json/testJson.json')