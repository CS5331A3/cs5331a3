import json

def load_potential_payloads():
	potential_payloads = []
	f = open("json/testExp.json").read()
	potential_payloads = json.loads(f)
	return potential_payloads

def load_injection_points():
	injection_points = []
	f = open("json/test2.json").read()
	injection_points = json.loads(f)
	return injection_points

def generate_payload_json(file1, exploits):
	jsonObj = {}
	jsonObj["exploits"] = exploits
	f = open(file1, 'w')
	jsonString = json.dumps(jsonObj, indent=4, sort_keys=True)
	f.write(jsonString)
	f.close()

def payload_generation(file1):
	potential_payloads = load_potential_payloads()
	injection_points = load_injection_points()

	pot_exploits = []

	for pot_pay in potential_payloads:

		for inject_point in injection_points :

			url = inject_point["url"]
			method = inject_point["method"]
			params = inject_point["params"]

			params_novalue = []
			params_value_dict = {}

			for param_k, param_v in params.iteritems():
				if param_v == '':
					params_novalue.insert(0, param_k)
				else:
					params_value_dict[param_k]=param_v
					print param_k
					

			if len(params_value_dict.keys()) > 0:
				for p_k, p_v in params.iteritems():
					exploit = {}
					exploit.update({"url":url})
					exploit.update({"method":method})
					newparams = {}
					for p in params_value_dict.keys():
						if p==p_k:
							newparams[p]=pot_pay
						else:
							newparams[p]=params[p]
					for pp_k in params_novalue:
						newparams[pp_k]=pot_pay
					exploit.update({"params":newparams})
					pot_exploits.insert(0, exploit)
			else:
				exploit = {}
				exploit.update({"url":url})
				exploit.update({"method":method})
				newparams = {}
				for pp_k in params_novalue:
					newparams[pp_k]=pot_pay
				exploit.update({"params":newparams})
				pot_exploits.insert(0, exploit)
					
	generate_payload_json(file1, pot_exploits)


payload_generation('json/test2w.json')