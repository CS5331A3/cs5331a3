# Imports
import injection
import json

# URL Global list
urlGlobalList = list()

# url
# url = 'http://www.wsb.com/Assignment2/case01.php'
url = 'http://www.wsb.com/Assignment2/case02.php'
# Prepare the value of the form which includes the attack payload
# value = dict(age=-1)

htmlContent = injection.get_html_content(url, value)
urlGlobalList = injection.parse_HTML(url, htmlContent)

# use dumps to write in json format
print json.dumps(urlGlobalList)